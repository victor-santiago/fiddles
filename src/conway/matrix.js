const Matrix = function(n = 5) {

	const size = n;
	let items = [];

	for(let i = 0; i < size; i++) {
		items.push([]);
		for(let j = 0; j < size; j++) {
			items[i].push(Math.random() > 0.5);
		}
	}

	const get = (row, col) => {
		if (row < 0 || row >= size || col < 0 || col > size) return null

		return items[row][col];
	}

	const adjacency = (row, col) => {
		const values = [];
		for (let i = row - 1; i < row + 2; i++) {
			for(let j = col - 1; j < col + 2; j++) {
				get(i, j) && values.push(items[i][j]);
			}
		}
		// Substract alive cells, they aren´t part of their adjacency!
		return values.reduce((a, b) => a + b, 0) - items[row][col];
	}

	return {

		transform() {
			const nextGen = []
			for(let i = 0; i < size; i++) {
				nextGen.push([])
				for(let j = 0; j < size; j++) {
					const alive = adjacency(i, j);

					if(items[i][j] && (alive === 2 || alive === 3)) {
						nextGen[i].push(true)
					} else if (!items[i][j] && alive === 3) {
						nextGen[i].push(true)
					} else {
						nextGen[i].push(false)
					}
				}
			}
			items = nextGen
			return this
		},

		// print() {
		// 	console.clear();
		// 	let str = "";
		// 	for (let i = 0; i < size; i++) {
		// 		str += "|";
		// 		for (let j = 0; j < size; j++) {
		// 			str += items[i][j] ? '*' : ' ';
		// 		}
		// 		str += "|\n"
		// 	}
		// 	console.log(str);
		// 	return this
		// },

		reset() {
			for(let i = 0; i < size; i++) {
				for(let j = 0; j < size; j++) {
					items[i][j] = Math.random() > 0.5;
				}
			}
			return this;
		}
	}
}

// window.m = new Matrix(100);

// m.print();

// setInterval(() => {
// 	m.transform().print();
// }, 300)
