const Cell = function (state = false) {

	let root = document.createElement('div');

	root.classList.add('cell');
	state && root.classList.add('is-active');

	return {
		onClick(fn) {
			root.addEventListener('click', (e) => fn(e));
			return this;
		},

		appendTo(el) {
			el.appendChild(root);
			return this;
		},

		activate() {
			root.classList.add('is-active');
			return this;
		},

		deactivate() {
			root.classList.remove('is-active');
			return this;
		},

		isActive() {
			return root.classList.contains('is-active');
		},

		toggleActivation() {
			if (this.isActive()) {
				this.deactivate();
			} else {
				this.activate();
			}
		}
	}
}

const Board = function (size = 4) {
	const root = document.createElement('div');
	root.classList.add('board');

	root.style.width = `${size * 2.5}rem`;
	root.style.height = `${size * 2.5}rem`;

	const cells = [];

	for(let i = 0; i < size; i++) {
		cells.push([]);
		for(let j = 0; j < size; j++) {
			let cell = new Cell(false);
			cells[i].push(cell);
			cell.appendTo(root).onClick(() => {cell.toggleActivation()});
		}
	}

	return {
		appendTo(el) {
			el.appendChild(root);
			return this;
		},

		getCells() {
			return cells;
		}
	}
}

window.board = new Board(21);

board.appendTo(document.querySelector('.app'));

