const uuid = (function(){
	let current = 0;

	return (preffix = '') => {
		current++;
		return preffix ? `${preffix}_${current - 1}` : current - 1;
	}
}())


const numberOfDigits = (number) => {
	return number > 0 ? Math.floor(Math.log10(number) + 1) : 1;
}

const getDigits = (number, length) => {
	return number.toString()
			.padStart(length, 0)
			.split('')
			.map(item => parseInt(item, 10))
}

const Digit = function (element, init = 0, speed = 350) {

	const id = uuid('digit');

	const template = `
		<div class="digit" id="${id}">
			<div class="base">
				${init}
			</div>
			<div class="flap over front">
				<div>${init}</div>
			</div>
			<div class="flap over back">
				<div>${init}</div>
			</div>
			<div class="flap under">
				<div>${init}</div>
			</div>
		</div>
	`;

	element.insertAdjacentHTML('beforeend', template);

	this.root = element.querySelector(`#${id}`);
	this.speed = speed;
	this.base = this.root.querySelector('.base')
	this.flapFront = this.root.querySelector('.flap.front');
	this.flapBack = this.root.querySelector('.flap.back');
	this.flapUnder = this.root.querySelector('.flap.under');
	this.flaps = this.root.querySelectorAll('.flap');

	this.flapFront.style.animationDuration = `${this.speed}ms`;
	this.flapBack.style.animationDuration = `${this.speed}ms`;

	return {
		jump: (from, to) => {

			// No real change, do nothing
			if(from === to) return;

			this.base.textContent = from;
			this.flapFront.querySelector('div').textContent = from;
			this.flapBack.querySelector('div').textContent = to;
			this.flapUnder.querySelector('div').textContent = to;

			this.flaps.forEach( flap => flap.style.display = 'block')

			setTimeout(() => {
				this.flaps.forEach( flap => flap.style.display = 'none' );
				this.base.innerHTML = to;
			}, this.speed + 50)
		},
		remove: () => {
			this.root.parentNode.removeChild(this.root);
		}
	}
}

const main = (number, clock) => {
	let digits = numberOfDigits(number);

	let figures = [];
	let figure;

	getDigits(number, digits).forEach( (digit, idx) => {
		figure = new Digit(clock, init = digit, speed = 270);
		figures.push(figure);
	});

	let current = number;
	let from = getDigits(current, digits);
	let to = getDigits(current -1, digits);

	const interval = setInterval(() => {
		figures.forEach( (item, idx) => {
			item.jump(from[idx], to[idx]);
		})

		current--;

		from = getDigits(current, digits);
		to = getDigits(current - 1, digits);

		if(current == 0) {
			clearInterval(interval);
		}
	}, 350)

	return [figures, interval]
}

;(function() {

	const input = document.querySelector('input');
	const restart = document.querySelector('button');
	const clock = document.querySelector('.clock');

	// Initialisation
	let [figures, interval] = main(input.value, clock);

	restart.addEventListener('click', () => {
		// Teardown
		interval && clearInterval(interval);
		figures.forEach(fig => fig.remove())
		figures =[];

		// Re-start
		[figures, interval] = main(input.value, clock);
	})

}())
