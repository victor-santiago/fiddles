# README #

This repository contains a copy of JSFiddles to show on my portfolio, including: 

1. [Flipclock: https://jsfiddle.net/viktor_santiago/6fyesv0d/](https://jsfiddle.net/viktor_santiago/6fyesv0d/)

Each fiddle has its own folder in this repo. Index.html files will let you run the fiddle in a standalone 
fashion in case you prefer to download the code instead of experience it at JSFiddle.
